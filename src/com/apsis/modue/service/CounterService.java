package com.apsis.modue.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/counterservice")
public class CounterService {
	
	// use the concurrent hashmap so that multiple requests can access the counters
	private static volatile Map<String,Integer> counterList = new ConcurrentHashMap<>();
	
	@Path("{counterName}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public String incrementCounter(@PathParam("counterName") String counterName) {
 
		String entry = counterList.entrySet().stream().filter(j -> counterName.
				     equals(j.getKey())).
				        map(i -> {i.setValue(i.getValue() + 1); return i.getKey() + " : " + i.getValue();}).collect(Collectors.joining(" : "));
 
		return entry;
	}
 
	@Path("{counterName}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getCurrentCounterValue(@PathParam("counterName") String counterName) {
		
		return counterList.entrySet().stream().filter(j -> counterName.
			     equals(j.getKey())).map(i -> i.getKey() + " : " + i.getValue()).collect(Collectors.joining(" : "));
	}
	
	
	@Path("all")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Integer> retrieveAllCounters() {
		
		return counterList;
	}
	
	// initialize the counter list with some counters
	static {
		
		counterList.put("counterA", 1);
		counterList.put("counterB", 1);
		counterList.put("counterC", 1);
		counterList.put("counterD", 1);
		counterList.put("counterE", 1);
	}

}
